package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button fancyButton = (Button) findViewById(R.id.button);

        fancyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //do fancy stuff
                System.out.println("Clicked!");
                Intent intent = new Intent(getApplicationContext(), AnotherActivity.class);
                startActivity(intent);
            }
        });
    }
}
