# Android tutorial

after you have installed the IDE (if not, you find the installation guide [here](https://gitlab.com/Laksuh/android-introduction/blob/master/installation-guide.md)) we are able to set up an Android project.

To start a new project, you need to press the "start a new Android Studio project" text on the "Welcome to Android Studio" window.
It'll ask you to choose from some app-templates. I'd recommend the "Empty Activity" one.

At the next screen you are asked for your applications name, the coding language and your minimum Android version.
If you've got a device with a lower android version than the latest and you want to code the app for your device, you need to select at least your current version.

F.e.
* Name: "My Application"
* language: Java
* Minimum API level: API 23 Android 6.0 (Marshmallow)

By clicking on finish, your project will be generated.

Android Studio provides you a hierarchy like this
![hierarchy picture][hierarchy]

[hierarchy]: https://gitlab.com/Laksuh/android-introduction/raw/master/hierarchy.png "hierarchy"

the following important folders contain the following files:

* manifest: AndroidManifest.xml --> let you define needed permissions and sets your app data (like name, logo, ...)
* java: your package with your source code
* res: following folders
 - drawable: pictures are dropped here
 - layout: your layouts are stored here
 - mipmap: contains your icon set
 - values: has some xml files with useable variables for your code, f.e. colors.xml, which defines the PrimaryColor as Green ( <color name="colorPrimary">#008577</color> )
* build.gradle (project:My Application): allows you to use additional build tools, like maven
* build.gradle (Module: app): allows you to set meta datas for your application, as version number or the used android verison and lets you add libraries/dependencies/sdks.

The use of buttons, layouts and additional context to understand programming Android applications, can be found in the [presentation file](https://gitlab.com/Laksuh/android-introduction/blob/master/ePortfolio-android-basics.pdf)

To test/run your application you can simply press the green "run"-symbol (triangle(play)) in the top bar or navigate to "Run->Run...".
But before you can do this, you need to set up a a virtual device or connect your physical device with your usb-cable to your computer.

##### Set up a virtual device
Navigate to "Tools->AVD Manager" and an additional window will open. On this you can manage different virtual devices. 
If you have an intel processor, you can simply press the "+ Create Virtual Device" button, select a phone, its Android Version (if you didn't already, you need to download it), enter a name and press "Finish".

If you have an AMD processor, you might have trouble under Windows (no problems with Linux or MacOS with a Hackintosh). There are a lot of forum posts out there with solving attempts.
None of them worked satisfying enough for me, which is why I recommend using your physical device or another virtual Device like [Nox](https://de.bignox.com) (this does just provide Android 7 for now (Oct 2019)). 

##### Connect a physical device
By simply plugging in your device, you can select your phone as a virtual device and test your application on your personal phone. But firstly you need to enable "USB-Debugging".

To do that, you need to get access to your "Developer Options". To achieve this, you need to navigate in your devices options to "About phone" and tap a few times on the "Build-Number"-label. A Toast/Message will pop up, which will tell you, that you are a developer now.

Now you have an additional option named "Developer Options" (This is different in every device, mostly you find it in "System").
There you can toggle "USB-Debugging" to test your application via your device.

### almost done to code 
If you selected the Empty Activity, as recommended, Android should've genereated you some files.
In your Java folder should be another folder hierarchy with your companys name (if you did not select one, it is "com.example.[NameOfYourProject]"). In there is your java source code and so your Activities located.
You should find at least (depending on the generated Activity you chose) one named "MainActivity", which contains code like this.


```java
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
    }
}
```

and another file in "res>layout" named "activity_main.xml", which contains some code like this.
```xml
<?xml version="1.0" encoding="utf-8"?>

<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:id="@+id/container"
    android:layout_width="match_parent"
    android:layout_height="match_parent" >

   <TextView
        android:id="@+id/textView"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Hello World!"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

We will start looking at the User Interface by looking at our XML-file.
In Android XML an UI-element is defined like this:
```xml
<Tag attribute0="value0"    
     attribute1="value1" >
</Tag>
```
The syntax is simple xml, like in HTML, but if you are still not willing to do this or just to lazy, Android Studio provides an UI-Builder, where you can chose from a huge variant of elements and simply drag and drop them on your view.
![uib][uib]

[uib]: https://gitlab.com/Laksuh/android-introduction/raw/master/Bildschirmfoto_2019-12-02_um_10.53.02.png "uib"

We will start by adding a button to your TextView (d&d it or write the code on your own).
You'll see, that you have some warnings, which is caused by missing constraints. You need to create constriants (at least one horizontal and one vertical) to other elements or the background layout/parent.
Otherwise your element jumps to x=0 and y=0. This is made, cause Android Devices are not having the same size at all and you want your UI to look nearly the same on each, different device.

To create a constraint, select the button and press one of the 4 circles around its borders. now you can draw a constraint to another element (f.e. the top one to the bottom of the TextView).
This indicates the button to position itself to this position (in the example, below the TextView, which is centered in the screen).

If you have chose a position, you can manipulate or add its attributes to costumize it as you like (in this tutorial it will stay default).

Your button-code should look smth. like this.

```xml
<Button
        android:id="@+id/button"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Button"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/textView" />
```

If you did everything correct, you can try out your application and see your results. You may notice, that the button has already animations, when clicking. 
This is caused by Android providing a lot of features on it's own and wanting you to keep it's design criterias. When you change to much from Android's provided idea, the button loses its animation and you need to set it on your own.

The design guidelines can be found [here](https://developer.android.com/design).

#### finally some coding
Now that we have initialized a TextView and a Button, we can start to add some functionality.

Therefore we navigate back to our MainActivity.java (where you should know now where it is located) and initialize our elements to our Activity.
To do this, we need to declare our elements as attributes in our java class. 
This works like the following. You need to set this attribute in your onCreate-Method (can be declared before, f.e. if the attribute need to be global) by saying your Activity, which id our Button has.
Cause we did not change any id, it is simply "button", which is why we can write de following.
```java
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Button fancyButton = findViewById(R.id.button);
        
    }
}
```

Now our Activity knows, that it has a button, and it is binded to the button-element on the activitys xml view.
So we do the same for our TextView (id should be "textView", if you didn't change it) --> try it on your own, before scrolling down and looking how it is done.


It should look smth. like this.
```java
    TextView fancyText = findViewById(R.id.textView);
```

Now we want some functionality. We want our application do some fancyStuff!
This works nearly similar to normal Java. the application is synchronus, but the user input (f.e. a button click) isn't. Therefore we need a ClickListener.
it is declared with the following syntax.

```java
fancyButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        //fancyStuffGoeshere
    }
});
```
In my example we want to change the TextViews Text, when the button is clicked.

Therefore we use the knowledge we have from Java before and set our TextViews text, when the button is clicked.

```java
fancyButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        //fancyStuffGoeshere
        textView.setText("Button Clicked! FancyStuff follows soon.")
    }
});
```

When everything worked out, you should see some fancy results, when testing our you application.

#### Cool, but what about an app with more than one screen?
As you may have noticed do the most useful apps have more than just one screen. WhatsApp f.e. does this with a TabLayout and additional Activites.
To call a second Activity you need to add a second Activity and call it like shown in the presentation (slide 11).
The rest is documented pretty well on Android Developers (the official Documentation by Google, I can recommend).


#### Links
The official Android documentation by Google can be found here:

[Android Developers](https://developer.android.com/guide)

Link to the [source code](https://gitlab.com/Laksuh/android-introduction/tree/master/app/)


