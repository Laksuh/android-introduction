# Installation guide

To develop something native in Android, it's the easiest way, to use Android Studio. This guide shows you how to set up the Android Studio IDE.

### Requirements

To run Android Studio your system (no matter if Windows, Linux, MacOS or Chrome OS) requires the Java Development Kit(JDK).
Luckily the latest Android Studio version comes bundled with a copy of the recommended OpenJDK, which is why you don't need to install it manually.
If you want to use higher features of Java than the one Android Studio comes with, you can manually install a JDK [here](https://java.com/en/download/help/windows_manual_download.xml).

The system requirements for your Computer are:
Windows 7/8/10, MacOS X 10.10, Ubuntu 14.04, every Chrome OS Version
4 GBs of RAM
2 GBs of disk space
minimum screen resolution of 1280x800
No CPU specifications, but to code fluently I'd recommend at least 4 Cores.

To run the Emulator you need a 64 Bit system and might have some troubles on Windows, when you do not have an Intel CPU. If you are f.e. using an Ryzen CPU, you can work arround the emulator, by installing an external one or simply using an Android smartphone device.

### Download and install Android Studio
[Latest Android Studio Version](https://developer.android.com/studio/?gclid=EAIaIQobChMIo_OEy9Ce5QIVA-R3Ch1XKQIKEAAYASAAEgLfFvD_BwE)

If you downloaded Android Studio, just click through the installation manager (it's not that hard, my grandma is literally able to handle this).

### Start a project

That was everything you need to set up if you want to develop an mobile application in Android.
A tutorial to start with a project, can be found [here](https://gitlab.com/Laksuh/android-introduction/blob/master/tutorial.md)