# Android ePortfolio

Repository for my ePortfolio about an Android-Tutorial. Here is a list with the content:

* [Installation guide](https://gitlab.com/Laksuh/android-introduction/blob/master/installation-guide.md)
* [The app](https://gitlab.com/Laksuh/android-introduction/tree/master/app)
* [Presentation](https://gitlab.com/Laksuh/android-introduction/blob/master/ePortfolio-android-basics.pdf)
* [Android Tutorial](https://gitlab.com/Laksuh/android-introduction/blob/master/tutorial.md)